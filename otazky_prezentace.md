# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 5-6 hodin |
| jak se mi to podařilo rozplánovat | dobře akorát příšte si dát více času na dokumentaci |
| design zapojení | [design zapojení](./dokumentace/design/shield_projekt_design.png) |
| proč jsem zvolil tento design | |
| zapojení | [zapojení](./dokumentace/schema/shield_schema.png) |
| z jakých součástí se zapojení skládá | 2x led, fotorezistor, teploměr, display, RGB pásek, prototypova deska, kolíkova lišta, vodiče, rezistory - 2x 220, 4,7k, 10k |
| realizace | [realizace](./dokumentace/fotky/finalni_s_displajem.png) |
| nápad, v jakém produktu vše propojit dohromady| |
| co se mi povedlo | realizace |
| co se mi nepovedlo/příště bych udělal/a jinak | nepoužil bych wemos |
| zhodnocení celé tvorby | až na kód velice dobré |