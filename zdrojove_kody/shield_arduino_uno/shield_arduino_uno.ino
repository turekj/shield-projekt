#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>

// Inicializace I2C LCD displeje
LiquidCrystal_I2C lcd(0x27, 16, 2);

  // Piny a nastavení pro LEDky
  #define LED1_PIN 11
  #define LED2_PIN 12

  // Piny pro LED pásek
  #define LED_STRIP_PIN 10
  #define NUM_LEDS 8

  // Piny pro fotosenzor a teplotní čidlo
  #define PHOTO_PIN A0
  #define TEMP_SENSOR_PIN 7

// Inicializace LED pásku
Adafruit_NeoPixel strip (NUM_LEDS, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

// Inicializace teplotního čidla DS18B20
OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature sensors(&oneWire);

int led = 0;
float temperatureC;
int photoValue;

void setup() {
  // Inicializace LCD displeje
  lcd.init();
  lcd.backlight();
  
  // Inicializace LEDek
  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);

  // Ujistí se, že jsou LEDky vypnuté
  digitalWrite(LED1_PIN, LOW);
  digitalWrite(LED2_PIN, LOW);
  
  // Inicializace LED pásku
  strip.begin();

  // Ujistí se, že jsou LEDky vypnuté
  strip.show(); 
  
  // Inicializace teplotního senzoru
  sensors.begin();
  
  
}

void senzor()  {
  // Čtení hodnoty z fotosenzoru
  photoValue = analogRead(PHOTO_PIN);

  // Čtení teploty z DS18B20
  sensors.requestTemperatures();
  temperatureC = sensors.getTempCByIndex(0);
}

void display()  {
  // Zobrazení hodnot na LCD
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Light: ");
  lcd.print(photoValue);
  
  lcd.setCursor(0, 1);
  lcd.print("Temp: ");
  lcd.print(temperatureC);
  lcd.print(" C");
}

void ledky()  {
  // Zapnutí LEDek
  led++;
  if (led % 2){
    digitalWrite(LED1_PIN, HIGH);
    digitalWrite(LED2_PIN, LOW);
  } else{
    digitalWrite(LED1_PIN, LOW);
    digitalWrite(LED2_PIN, HIGH);
  }
}

void pasek()  {
  // Blikání LED pásku s náhodnými barvami
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color(random(0, 255), random(0, 255), random(0, 255)));
  }
  strip.show();
  delay(random(100, 1000));
}

void loop() {

  senzor();

  display();

  ledky();

  pasek();

}
